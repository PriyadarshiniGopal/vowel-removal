﻿using System;

namespace vowel_removal
{
    class Program
    {
        static void Main(string[] args)
        {
            int n;
            Console.WriteLine("Enter no of words");
            try
            {
                n = Convert.ToInt32(Console.ReadLine());
            }
            catch
            {
                Console.WriteLine("Null values not accepted ! Enter a word");
                n = Convert.ToInt32(Console.ReadLine());
            }
            String[] words = new String[n];
            String[] vowels = new String[] { "a", "e", "i", "o", "u", "A", "E", "I", "O", "U" };
            Console.WriteLine("Enter words one by one");
            for (int i = 0; i < n; i++)
            {
                words[i] = Console.ReadLine();
                if (words[i] == "")
                {
                    Console.WriteLine("Null values not accepted ! Enter a word");
                    i--;
                }
            }
            Console.WriteLine("Words after removed vowels");
            for (int i = 0; i < n; i++)
            {
                for(int j=0;j<vowels.Length;j++)
                words[i] = words[i].Replace(vowels[j], "");
                Console.WriteLine(words[i]);
            }
        }
    }
}
